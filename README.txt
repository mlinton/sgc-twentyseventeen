* Twenty Seventeen ===
Contributors: wordpressdotorg, matthewlinton
Requires at least: WordPress 4.7, Simple Golf Club 1.5
Tested up to: WordPress 5.0
Requires PHP: 5.2.4
Version: 2.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: one-column, two-columns, right-sidebar, flexible-header, accessibility-ready, custom-colors, custom-header, custom-menu, custom-logo, editor-style, featured-images, footer-widgets, post-formats, rtl-language-support, sticky-post, theme-options, threaded-comments, translation-ready

== Description ==

SGC Twenty Seventeen provides sample code to implement the public functions found in Simple Golf Club (https://bitbucket.org/matthewlinton/simplegolfclub/). It's based on the Wordpress theme Twenty Seventeen (https://wordpress.org/themes/twentyseventeen/).

For more information about SGC Twenty Seventeen please go to https://bitbucket.org/matthewlinton/sgc-twentyseventeen/

== Installation ==

1. unzip the contents of the theme into your wordpress themes folder

== Copyright ==

SGC Twenty Seventeen WordPress Theme, Copyright 2016 Matthew Linton
SGC Twenty Seventeen is distributed under the terms of the GNU GPL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

SGC Twenty Seventeen bundles the following third-party resources:

HTML5 Shiv, Copyright 2014 Alexander Farkas
Licenses: MIT/GPL2
Source: https://github.com/aFarkas/html5shiv

jQuery scrollTo, Copyright 2007-2015 Ariel Flesler
License: MIT
Source: https://github.com/flesler/jquery.scrollTo

normalize.css, Copyright 2012-2016 Nicolas Gallagher and Jonathan Neal
License: MIT
Source: https://necolas.github.io/normalize.css/

Font Awesome icons, Copyright Dave Gandy
License: SIL Open Font License, version 1.1.
Source: http://fontawesome.io/

Bundled header image, Copyright Alvin Engler
License: CC0 1.0 Universal (CC0 1.0)
Source: https://unsplash.com/@englr?photo=bIhpiQA009k

== Changelog ==

= 1.0 =
* Released: May 7, 2019

Initial Release based on v2.2 of the Wordpress theme Twenty Seventeen
https://codex.wordpress.org/Twenty_Seventeen_Theme_Changelog#Version_2.2
