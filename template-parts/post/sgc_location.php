<?php
/**
 * Template part for displaying sgc_location posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
	if ( is_sticky() && is_home() ) :
		echo twentyseventeen_get_svg( array( 'icon' => 'thumb-tack' ) );
	endif;
	?>
	<header class="entry-header">
		<?php
		if ( 'post' === get_post_type() ) {
			echo '<div class="entry-meta">';
			if ( is_single() ) {
				twentyseventeen_posted_on();
			} else {
				echo twentyseventeen_time_link();
				twentyseventeen_edit_link();
			};
			echo '</div><!-- .entry-meta -->';
		};

		if ( is_single() ) {
			the_title( '<h1 class="entry-title">', '</h1>' );
		} elseif ( is_front_page() && is_home() ) {
			the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
		} else {
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		}
		?>
	</header><!-- .entry-header -->

        <!-- ####### BEGIN Simple Golf Club example code ################### -->
        <!-- The following section provides examples of calls to public PHP
             functions available for retrieving information about SGC_Location posts -->
        <?php if( function_exists( 'sgc_location_getinfo' ) && is_single() ) : ?>
        <div class="sgc-section-title"><h2>PHP and JSON API</h2></div>
        <div class="sgc_locationinfo">
            <div>
                <h3>sgc_location_getinfo( <small>['location_id']</small> )</h3>
                <?php $rest_url = get_option('siteurl') . '/wp-json/simplegolfclub/v1/location/info/' . get_the_id(); ?>
                <a href="<?= $rest_url ?>" target="events_url"><?= $rest_url ?></a>
                <pre><?php print_r( sgc_location_getinfo( ) ); ?></pre>
            </div>
            <br>
            <div>
                <h3>sgc_location_gettees( <small>['location_id']</small> )</h3>
                <?php $rest_url = get_option('siteurl') . '/wp-json/simplegolfclub/v1/location/tees/' . get_the_id(); ?>
                <a href="<?= $rest_url ?>" target="events_url"><?= $rest_url ?></a>
                <pre><?php print_r( sgc_location_gettees( ) ); ?></pre>
            </div>
            <br>
            <div>
                <h3>sgc_location_getevents( <small>['location_id']</small> )</h3>
                <?php $rest_url = get_option('siteurl') . '/wp-json/simplegolfclub/v1/location/events/' . get_the_id(); ?>
                <a href="<?= $rest_url ?>" target="events_url"><?= $rest_url ?></a>
                <pre><?php print_r( sgc_location_getevents( ) ); ?></pre>
            </div>
        </div>
        <?php endif; ?>
        <!-- ####### END Simple Golf Club example code #################### -->
        
	<?php if ( '' !== get_the_post_thumbnail() && ! is_single() ) : ?>
		<div class="post-thumbnail">
			<a href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail( 'twentyseventeen-featured-image' ); ?>
			</a>
		</div><!-- .post-thumbnail -->
	<?php endif; ?>
        <!-- ####### END Simple Golf Club example code #################### -->
                
	<div class="entry-content">
		<?php
		/* translators: %s: Name of current post */
		the_content(
			sprintf(
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen' ),
				get_the_title()
			)
		);

		wp_link_pages(
			array(
				'before'      => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
				'after'       => '</div>',
				'link_before' => '<span class="page-number">',
				'link_after'  => '</span>',
			)
		);
		?>
	</div><!-- .entry-content -->

	<?php
	if ( is_single() ) {
		twentyseventeen_entry_footer();
	}
	?>

</article><!-- #post-<?php the_ID(); ?> -->
